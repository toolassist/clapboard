FROM node:8

WORKDIR /usr/src/app

# The command to run
EXPOSE 8080
CMD ["node", "index.js"]
