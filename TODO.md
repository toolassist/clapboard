# Donations page
Move annonymous checkbox to top of form
Move actual checkbox in front of text for anonymous
Add Name to form
Add placeholder to Name Input: "Anonymous"
Add default-value to Name Input: "Anonymous" - backend
Add small text to Name input for disclosure
Make Comment Optionional
Add disclosure to Comment about optional
Inline Labels (with quantifiers)
Labels should be equal width
"Pay with" label

# Admin page
Set default donation amount
Set minimum donation amount
default donation amount can not be lower than minimum

Select supported Payment Gateways
Configure said supported gateways
Set Default Gateway

Legal disclaimer entry something another
