/**
 * Module dependencies.
 */
import * as modules from "./modules/";
import { EventBus } from "./modules/eventBus";

const moduleLoader = async () => {
	const promises = Object.keys(modules).map((moduleName) => {
		if (moduleName === "Core") {
			modules[moduleName].getInstance();
		} else {
			new modules[moduleName];
		}
		console.info(`${moduleName} Loaded`);
	});
	await Promise.all(promises)
};

moduleLoader().then(() => {
	EventBus.emit("message", "modules loaded");
});
