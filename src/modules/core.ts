import { EventBus } from "./eventBus";
import { ORM, Waterline } from "./orm";
import { TwitchClient } from 'irc';


interface TwitchHandler {
	id: string,
	handler: () => void
}

export class Core {
	private static instance: Core;

	public _config: any = {};
	public _orm: any;
	public Waterline = Waterline;
	private twitchClient: TwitchClient = new TwitchClient();
	private twitchHandlers: TwitchHandler[];

	constructor() {
		EventBus.on("register", this.register.bind(this));
		EventBus.on("message", this.handleMessage.bind(this));
		EventBus.on("subscribe", this.subscribe.bind(this));
		EventBus.on("unsubscribe", this.unsubscribe.bind(this));
		this.twitchClient.connect();
		this.twitchHandlers = [];
	}

	static getInstance() {
		if (!Core.instance) {
			Core.instance = new Core();
			Core.instance._orm = new ORM();

			const registration = {
				name: "CORE",
				target: "Core",
				model: {
					config: {
						datastore: 'default',
						primaryKey: 'property',
						attributes: {
							property: {type: "string", required: true},
							value: {type: "string"}
						}
					}
				}
			};

			EventBus.emit("register", registration);
		}

		return Core.instance;
	}

	register(module) {
		if (module.target === "Core") {
			if (module.model) {
				this._orm.registerModel(module.model);
			}
			console.info(`${module.name} just registered with CORE`);
		}
	}

	handleMessage(message) {
		if (message === "modules loaded") {
			console.info(`All Modules Loaded`);
			this._orm.start().then(() => {
				EventBus.emit('message', 'CORE started');
			});
		}
	}

	subscribe(payload) {
		const joined = this.twitchClient.joinChannel(payload.channel);
		if (joined) {
			this.twitchHandlers.push({
				id: payload.id,
				handler: payload.handler
			});
			this.twitchClient.on('message', payload.handler)
		}
	}

	unsubscribe(payload) {
		this.twitchClient.leaveChannel(payload.channel);
		this.twitchHandlers
			.filter((handler) => {
				return handler.id === payload.id;
			})
			.forEach((handler) => {
				this.twitchClient.off('message', handler.handler)
			});
	}

	getConfig(property) {

		return this._orm.getModel("config").findOne({
			where: {
				property: property
			}
		}).then(async (config) => {
			return config && config.value || null;
		});
	}

	setConfig(property, value) {
		return this._orm.getModel("config").findOrCreate({
			property: property
		},{
			property: property,
			value: value
		}).exec(async (err, user, wasCreated) => {
			if (err) {return err}

			if (!wasCreated) {
				return this._orm.getModel("config").updateOne({
					property: property
				}).set({
					property: property,
					value: value
				});
			}
		});
	}
}
