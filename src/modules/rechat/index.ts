import * as path from "path";
import * as request from "request";
import {EventBus} from "../eventBus";
import {Core} from "../core";
import * as uuid from "uuid/v4";
import * as escapeHtml from 'escape-html';

export class ReChat {

	private core: Core;
	private channels: object = {};
	private _showEmotes: boolean;
	private _showBadges: boolean;
	private _showCPM: boolean;

	constructor() {

		const registration = {
			name: "rechat",
			target: "WebServer",
			statics: [
				{
					path: '/static',
				}
			],
			routes: [
				{
					method: 'get',
					path: '/admin/rechat',
					action: this.rechatAdmin.bind(this)
				},
				{
					method: 'get',
					path: '/widgets/rechat/:user',
					action: this.rechat.bind(this)
				},
				{
					method: 'ws',
					path: '/sockets/rechat/:user',
					action: this.socket.bind(this)
				},
				{
					method: 'get',
					path: '/api/rechat/badges/:badge/:version?',
					action: this.badges.bind(this)
				},
				{
					method: 'get',
					path: '/api/rechat/config',
					action: this.getConfig.bind(this)
				},
				{
					method: 'patch',
					path: '/api/rechat/config',
					action: this.patchConfig.bind(this)
				}
			]
		};

		EventBus.emit("register", registration);
		EventBus.on("message", (message) => {
			if (message === "CORE started") {
				this.core = Core.getInstance();
				this.core.getConfig('rechat:showEmotes').then((value) => {
					this.showEmotes = (value === "true");
				});
				this.core.getConfig('rechat:showBadges').then((value) => {
					this.showBadges = (value === "true");
				});
				this.core.getConfig('rechat:showCPM').then((value) => {
					this.showCPM = (value === "true");
				});
			}
		});
	}

	get showEmotes():boolean {
		return this._showEmotes;
	}

	set showEmotes(value:boolean) {
		this._showEmotes = value;
		this.core.getConfig('rechat:showEmotes').then((dbValue) => {
			if (dbValue !== value) {
				this.core.setConfig('rechat:showEmotes', value);
			}
		});
	}

	get showBadges():boolean {
		return this._showBadges;
	}

	set showBadges(value:boolean) {
		this._showBadges = value;
		this.core.getConfig('rechat:showBadges').then((dbValue) => {
			if (dbValue !== value) {
				this.core.setConfig('rechat:showBadges', value);
			}
		});
	}

	get showCPM():boolean {
		return this._showCPM;
	}

	set showCPM(value:boolean) {
		this._showCPM = value;
		this.core.getConfig('rechat:showCPM').then((dbValue) => {
			if (dbValue !== value) {
				this.core.setConfig('rechat:showCPM', value);
			}
		});
	}

	patchConfig(req, res) {
		const config = req.body;
		this.showEmotes = config.showEmotes;
		this.showBadges = config.showBadges;
		this.showCPM = config.showCPM;

		res.status(202);
		res.send();
	}

	getConfig(req, res) {
		res.send({
			showEmotes: this.showEmotes,
			showBadges: this.showBadges,
			showCPM: this.showCPM
		});
	}

	rechatAdmin(req, res) {
		res.sendFile(path.join(__dirname + '/admin/index.html'));
	}

	rechat(req, res) {
		res.sendFile(path.join(__dirname + '/200.html'));
	}

	badges(req, res) {
		request(`https://badges.twitch.tv/v1/badges/global/display`, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				let badge_sets = JSON.parse(body).badge_sets;
				let versions = badge_sets[req.params.badge] && badge_sets[req.params.badge].versions;
				let image = versions &&
					(req.params.version &&
					versions[req.params.version] ?
						versions[req.params.version].image_url_1x :
						versions[Object.keys(versions).slice(-1)[0]].image_url_1x) ||
					null;
				if (!!image) {
					request(image).pipe(res);
				} else {
					res.status(404);
					res.send();
				}
			}
		});
	}

	socket(ws, req) {
		ws.id = uuid();
		const channel = req.params.user;
		this.channels[channel] = this.channels[channel] || {
			sockets: []
		};

		this.channels[channel].sockets.push(ws);

		ws.on('close', () => {
			this.channels[channel].sockets.splice(this.channels[channel].sockets.findIndex((socket)=>{
				return socket === ws;
			}), 1);

			if (this.channels[channel].sockets.length === 0) {
				this.leaveChannel(channel, ws.id);
			}
		});

		this.joinChannel(channel, ws.id);
	}

	joinChannel(channel, id) {
		EventBus.emit("subscribe", {
			id: id,
			channel: '#'+channel,
			handler: (message) => {
				if (message.channel === '#'+channel && message.command === 'PRIVMSG') {
					console.log(message.command, message.channel, message.sender, message.message);
					this.handleMessage(channel, message);
					this.channels[channel].messages.total++;
				}
			}
		});
		this.channels[channel].messages = {
			start: 0,
			second: 0,
			minute: 0,
			total: 0
		};
		this.channels[channel].messages.start = Date.now();
		setInterval(() => {
			const elapsed = Date.now() - this.channels[channel].messages.start;
			this.channels[channel].messages.second = Math.floor(this.channels[channel].messages.total / Math.ceil(elapsed/1000));
			this.channels[channel].messages.minute = Math.floor(this.channels[channel].messages.total / Math.ceil(elapsed/60000));
			this.channels[channel].sockets.forEach((socket) => {
				socket.send(JSON.stringify({
					type: "info",
					data: this.channels[channel].messages
				}));
			});
		}, 1000);
	}

	leaveChannel(channel, id) {
		EventBus.emit("unsubscribe", {
			id: id,
			channel: '#'+channel
		});
	}

	handleMessage(channel, message) {
		if (this.showEmotes) {
			const originalMessage = message.parameters[1];
			const emotes = message.tags.emotes.split('/');
			const replaceEmotes = [];
			if (message.tags.emotes !== "") {
				emotes.forEach(function (emote) {
					const emoteID = emote.split(':')[0];
					const emoteLocation = emote.split(':')[1].split(',')[0];
					const replaceText = originalMessage.substring(parseInt(emoteLocation.split('-')[0]), parseInt(emoteLocation.split('-')[1]) + 1);
					replaceEmotes.push({
						emoteID,
						replaceText
					})
				});
			}
			message.escapedMessage = escapeHtml(message.parameters[1]);

			replaceEmotes.forEach((emote) => {
				const regex = new RegExp(`(?<=\\s|^)${emote.replaceText.replace(/[\\\(\)\[\]\?]/g, "\\$&")}(?=\\s|$)`, 'g');
				message.escapedMessage = message.escapedMessage.replace(
					regex,
					`<img src="https://static-cdn.jtvnw.net/emoticons/v1/${emote.emoteID}/1.0" />`
				);
			});
		} else {
			message.escapedMessage = escapeHtml(message.parameters[1]);
		}

		this.channels[channel].sockets.forEach((socket) => {
			socket.send(JSON.stringify({
				type: "message",
				data: message
			}));
		});
	}
}
