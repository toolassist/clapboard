import React, { PureComponent } from "react";
import './CPM.css';

export default class CPM extends PureComponent {

	constructor(props) {
		super(props);
		this.state = {};
		const pathArray = window.window.location.pathname.split('/');
		const socket = new WebSocket(
			'ws://' + window.location.hostname +
			(window.location.port ? ':' + window.location.port : '') +
			'/sockets/rechat/' + pathArray[3]);
		socket.onmessage = this.handleMessage.bind(this);
	}

	handleMessage(event) {
		const data = JSON.parse(event.data);
		if (data.type === 'info') {
			console.debug(data.data);
			this.setState(data.data);
		}
	}

	render() {
		if (!this.props.showCPM) return false;

		return (
			<div className="CPM">
				{this.state.second}
			</div>
		);
	}
}
