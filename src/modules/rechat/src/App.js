import React, { PureComponent } from "react";
import * as request from "request-promise-native";
import CPM from "./CPM";
import './App.css';

export default class App extends PureComponent {

	apiHost;
	state;
	messagesList;
	odd = true;

	constructor(props) {
		super(props);
		this.apiHost = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
		this.state = {};
		request(`${this.apiHost}/api/rechat/config`)
			.then((response) => {
				this.setState(JSON.parse(response));
				console.debug(this.state);
			})
			.catch((error) => {
				console.error(error);
			});

		const pathArray = window.window.location.pathname.split('/');
		const socket = new WebSocket(
			'ws://' + window.location.hostname +
			(window.location.port ? ':' + window.location.port : '') +
			'/sockets/rechat/' + pathArray[3]);
		socket.onmessage = this.handleMessage.bind(this);
	}

	componentDidMount() {
		this.messagesList = document.getElementById('messages');
		this.messagesList.style.paddingRight = this.messagesList.offsetWidth - this.messagesList.clientWidth + 'px';
	}

	handleMessage(event) {
		const data = JSON.parse(event.data);
		if (data.type === 'message') {
			const message = data.data;
			let background;
			if (this.odd) {
				background = 'rgba(255,255,255,0)';
			} else {
				background = 'rgba(255,255,255,0.1)';
			}
			this.odd = !this.odd;

			if (this.state.showBadges) {
				const badgeObject = message.tags.badges.split(',');
				if (message.tags.badges !== '' && badgeObject.length > 0) {
					let badges = '';
					badgeObject.forEach((badge) => {
						const badgeName = badge.split('/')[0];
						const badgeVersion = badge.split('/')[1];
						badges = badges + `<img alt="" src="${this.apiHost}/api/rechat/badges/${badgeName}/${badgeVersion}" onerror="this.style.display='none'" />`;
					});
					this.messagesList.innerHTML += `<li class="received" style="background: ${background}">${badges}<span style="${message.tags.color !== true ? 'color:' + message.tags.color : ''}">${message.tags['display-name'] || message.prefix.username}</span>: <span>${message.escapedMessage}</span></li>`;
					this.updateScroll();
				} else {
					this.messagesList.innerHTML += `<li class="received" style="background: ${background}"><span style="${message.tags.color !== true ? 'color:' + message.tags.color : ''}">${message.tags['display-name'] || message.prefix.username}</span>: <span>${message.escapedMessage}</span></li>`;
					this.updateScroll();
				}
			} else {
				this.messagesList.innerHTML += `<li class="received" style="background: ${background}"><span style="${message.tags.color !== true ? 'color:' + message.tags.color : ''}">${message.tags['display-name'] || message.prefix.username}</span>: <span>${message.escapedMessage}</span></li>`;
				this.updateScroll();
			}
		}
	}

	updateScroll() {
		const items = this.messagesList.childNodes;
		items[items.length - 1].scrollIntoView();
		items.forEach((item) => {
			const itemRect = item.getBoundingClientRect();
			if (itemRect.top < (0 - item.offsetHeight)) {
				item.remove();
			}
		});
	}

	render() {
		return (
			<div className="App">
				<CPM showCPM={this.state.showCPM}/>
				<ul id="messages"></ul>
			</div>
		);
	}
}
