import * as express from "express";
import {Express} from "express";
import * as bodyParser from "body-parser";
import * as expressWebSockets from "express-ws";
import * as colors from "colors/safe";
import {EventBus} from "../eventBus";
import {Core} from "../core";
import * as path from "path";

type Route = {
	method: string
	path: string
	action(): void
}

type Static = {
	path: string
}

export class WebServer {

	core: Core;
	server: Express;
	registrations: Route[];
	statics: Static[];

	constructor() {

		this.statics = [];
		this.registrations = [];

		const registration = {
			name: "WebServer",
			target: "Core"
		};

		EventBus.emit("register", registration);
		EventBus.on("message", (message) => {
			if (message === "CORE started") {
				this.core = Core.getInstance();
				this.core.getConfig('web_server_port').then((value) => {
					const config: any = {};
					config.host = '0.0.0.0';
					config.port = process.env.WEB_PORT || value || '1234';
					this.core.setConfig('web_server_port', config.port);

					this.startServer(config);
				});
			}
		});

		EventBus.on("register", this.register.bind(this));
	}

	register(registration) {
		if (registration.target==="WebServer") {
			if (!registration.routes) {
				console.log(`${colors.red(`ERROR:`)} ${registration.name} does not contain a routes array.`);
			}

			if (registration.statics) {
				registration.statics.map((staticConfig) => {
					staticConfig.module = registration.name;
					return staticConfig;
				})
			}

			if (!this.server) {
				registration.statics && registration.statics.forEach((staticConfig) => {
					this.statics.push(staticConfig);
				});
				registration.routes.forEach((route) => {
					this.registrations.push(route);
				});
			} else {
				registration.statics && registration.statics.forEach(this.registerStatic.bind(this));
				registration.routes.forEach(this.registerRoute.bind(this));
			}
		}
	};

	startServer(config) {
		this.server = express();

		expressWebSockets(this.server);

		this.server.use(bodyParser.json());

		this.statics.forEach(this.registerStatic.bind(this));
		this.registrations.forEach(this.registerRoute.bind(this));

		this.server.listen(config.port, config.host, () => {
			console.log(`WebServer listening on ${colors.cyan(config.port.toString())}`);
			EventBus.emit("message", "WebServer Started");
		});
	}

	registerStatic(staticConfig) {
		console.log(`${colors.blue("Static Loaded:")} ${colors.yellow(staticConfig.module)} - ${staticConfig.path}`);
		this.server.use('/static', express.static(path.join(__dirname,`../${staticConfig.module}${staticConfig.path}`)));
	}

	registerRoute(route) {
		console.log(`${colors.cyan("Route Loaded:")} ${colors.yellow(route.method.toUpperCase())} - ${route.path}`);
		this.server[route.method](route.path, route.action);
	}
}
